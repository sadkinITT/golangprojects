package main

import "fmt"

type employees struct {
	firstname string
	lastname  string
	age       int
	email     string
}

func (e employees) giveDetails() {
	fmt.Println("Employee name is:", e.firstname, " ", e.lastname,
		"whose age is", e.age, "and the email id of employee is", e.email)
}

func main() {
	var e [5]employees
	e[0] = employees{"Kingsuk", "Das", 24, "kingsuk.das@itt.com"}
	e[1] = employees{"Kingsuk", "Das", 24, "kingsuk.das@itt.com"}
	e[2] = employees{"Kingsuk", "Das", 24, "kingsuk.das@itt.com"}
	e[3] = employees{"Kingsuk", "Das", 24, "kingsuk.das@itt.com"}
	e[4] = employees{"Kingsuk", "Das", 24, "kingsuk.das@itt.com"}
	for i := 0; i < 5; i++ {
		//fmt.Println(e[i])
		e[i].giveDetails()
	}
	//e.giveDetails()
}
