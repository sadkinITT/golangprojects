package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

)

type employee struct {
	ID    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	EmpID string        `json:"empid"`
	Name  string        `json:"name"`
	Email string        `json:"email" `
}

func home(w http.ResponseWriter, r *http.Request) {
	sesson, err := mgo.Dial("localhost:27017")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("connected")
	}
	defer sesson.Close()
	sesson.SetMode(mgo.Monotonic, true)
	c := sesson.DB("testdb").C("Epmloyee")

	iter := c.Find(nil).Iter()
	result := employee{}

	for iter.Next(&result) {
		res, err := json.Marshal(result)
		if err != nil {
			log.Fatal(err)
		} else {
			fmt.Println(string(res))
			io.WriteString(w, string(res))
		}
	}

}
func main() {

	// for iter.Next(&result) {
	// 	toMap(result)
	// }

	http.HandleFunc("/", home)
	http.ListenAndServe(":4065", nil)
}

/*
func toMap(result employee) {
	data := map[string][]string{
		result.EmpID: []string{result.Name, result.Email},
	}
	fmt.Println(data[result.EmpID])

*/
//update
/*err = c.Update(bson.M{"name": "Larry"}, bson.M{"$set": bson.M{"name": "May"}})
if err != nil {
	log.Fatal(err)
}*/
