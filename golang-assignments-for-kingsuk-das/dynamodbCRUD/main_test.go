package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"github.com/dynamodbCRUD/library"
	"github.com/dynamodbCRUD/mock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

var mock1 *mock.DynaMock

func init() {
	//Dyna := new(library.MyDynamo)
	//var Db dynamodbiface.DynamoDBAPI
	_, mock1 = mock.New()
}

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/library", library.Handlers).Methods("POST")
	return router
}

func GRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/library", library.GetHandlers).Methods("GET")
	return router
}

func GMRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/library/1", library.GetHandlers).Methods("GET")
	return router
}

func TestInsertBook(t *testing.T) {
	m := mux.NewRouter()
	m.HandleFunc("/library", library.Handlers).Methods("POST")
	book := library.ReturnBook()
	jsonbook, errJsonParsing := json.Marshal(book)
	if errJsonParsing != nil {
		panic(errJsonParsing)
	}
	request, _ := http.NewRequest("POST", "/library", bytes.NewBuffer(jsonbook))
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 200, response.Code, "response is not correct")
}

func TestGetBook(t *testing.T) {
	m := mux.NewRouter()
	m.HandleFunc("/library", library.Handlers).Methods("GET")
	request, _ := http.NewRequest("GET", "/library", nil)
	response := httptest.NewRecorder()
	GRouter().ServeHTTP(response, request)
	//l := &library.GetLibrary{BookID: "137", BookName: "Go for web", BookAuthor: "DB"}
	assert.Equal(t, 200, response.Code, "response is not correct")
}

func TestGetBookMock(t *testing.T) {
	expectKey := map[string]*dynamodb.AttributeValue{
		"BookID": {
			S: aws.String("1"),
		},
	}
	result := dynamodb.GetItemOutput{
		Item: map[string]*dynamodb.AttributeValue{
			"BookID": {
				S: aws.String("1"),
			},
			"BookName": {
				S: aws.String("abc"),
			},
			"BookAuthor": {
				S: aws.String("k"),
			},
		},
	}

	mock1.ExpectGetItem().ToTable("Library").WithKeys(expectKey).WillReturns(result)
	m := mux.NewRouter()
	m.HandleFunc("/library/1", library.GetHandlers).Methods("GET")
	request, _ := http.NewRequest("GET", "/library/1", nil)
	response := httptest.NewRecorder()
	GMRouter().ServeHTTP(response, request)
	//l := &library.GetLibrary{BookID: "1", BookName: "abc", BookAuthor: "k"}
	fmt.Println(response.Code)
	//assert.Equal(t, *l, response.Body.String(), "response is not correct")
	assert.Equal(t, 200, response.Code, "Response not ok")
}

// func TestDeleteBook(t *testing.T) {
// 	m := mux.NewRouter()
// 	m.HandleFunc("/library?BookID=137", library.Handlers).Methods("DELETE")
// 	request, _ := http.NewRequest("DELETE", "/library?BookID=137", nil)
// 	response := httptest.NewRecorder()
// 	m.ServeHTTP(response, request)
// 	assert.Equal(t, 200, response.Code, "Deletion not successful")
// }
