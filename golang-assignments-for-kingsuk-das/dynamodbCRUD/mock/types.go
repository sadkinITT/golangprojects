package mock

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

type (
	// MockDynamoDB struct hold DynamoDBAPI implementation and mock object
	MockDynamoDB struct {
		dynamodbiface.DynamoDBAPI
		dynaMock *DynaMock
	}

	// DynaMock mock struct hold all expectation types
	DynaMock struct {
		GetItemExpect []GetItemExpectation
		// BatchGetItemExpect       []BatchGetItemExpectation
		// UpdateItemExpect         []UpdateItemExpectation
		// PutItemExpect            []PutItemExpectation
		// DeleteItemExpect         []DeleteItemExpectation
		// BatchWriteItemExpect     []BatchWriteItemExpectation
		// CreateTableExpect        []CreateTableExpectation
		// DescribeTableExpect      []DescribeTableExpectation
		// WaitTableExistExpect     []WaitTableExistExpectation
		// ScanExpect               []ScanExpectation
		// QueryExpect              []QueryExpectation
		// TransactWriteItemsExpect []TransactWriteItemsExpectation
	}

	// GetItemExpectation struct hold expectation field, err, and result
	GetItemExpectation struct {
		table  *string
		key    map[string]*dynamodb.AttributeValue
		output *dynamodb.GetItemOutput
	}
)
