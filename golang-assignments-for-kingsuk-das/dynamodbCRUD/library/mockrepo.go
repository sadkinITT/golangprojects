package library

import (
	"github.com/dynamodbCRUD/mock"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// MyDynamo struct hold dynamodb connection
// type MyDynamo struct {
// 	Db dynamodbiface.DynamoDBAPI
// }

// Dyna - object from MyDynamo
//var Dyna *MyDynamo

// ConfigureDynamoDB - init func for open connection to aws dynamodb
//func ConfigureDynamoDB() {
//Dyna = new(MyDynamo)
//awsSession, _ := session.NewSession(&aws.Config{Region: aws.String("ap-south-1")})
//svc := dynamodb.New(awsSession)
//Dyna.Db = dynamodbiface.DynamoDBAPI(svc)

//}

var Db *mock.MockDynamoDB

// GetBook - example func using GetItem method
func GetBookMockByID(id string) ([]GetLibrary, error) {
	Db, _ = mock.New()
	parameter := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"BookID": {
				N: aws.String(id),
			},
		},
		TableName: aws.String("Library"),
	}
	var l []GetLibrary
	response, err := Db.GetItems(parameter)
	if err != nil {
		return nil, err
	}
	lib := GetLibrary{}
	if err := dynamodbattribute.UnmarshalMap(response.Item, &lib); err != nil {
		return nil, err
	}
	l = append(l, lib)
	return l, nil
}

// func GetBookMock() ([]GetLibrary, error) {
// 	Db,_ = mock.New()
// 	parameter := &dynamodb.ScanInput{
// 		TableName: aws.String("Library"),
// 	}

// 	response, err := Db.Scan(parameter)
// 	if err != nil {
// 		return nil, err
// 	}
// 	var l []GetLibrary
// 	for _, i := range response.Items {
// 		lib := GetLibrary{}

// 		err := dynamodbattribute.UnmarshalMap(i, &lib)

// 		if err != nil {
// 			fmt.Println("Got error unmarshalling:")
// 			fmt.Println(err.Error())
// 			os.Exit(1)
// 		}
// 		l = append(l, lib)
// 	}
// 	return l, nil
// }
