package main

import (
	"fmt"

	calculator "github.com/Problem4Week2/Calculator"
)

func main() {
	var a int = 3
	var b int = 5
	fmt.Println("The addition result:", calculator.Add(a, b))
	fmt.Println("The substraction result", calculator.Subs(a, b))
	fmt.Println("The multiplication result", calculator.Multiplication(a, b))
	fmt.Println("The division result", calculator.Division(a, b))
}
