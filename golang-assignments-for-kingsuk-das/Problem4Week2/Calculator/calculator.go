package calculator

func Add(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}

func Subs(args ...int) int {
	subresult := 0
	for _, v := range args {
		subresult -= v
	}
	return subresult
}

func Multiplication(args ...int) int {
	result := 1
	for _, v := range args {
		result *= v
	}
	return result
}

func Division(args ...int) int {
	result := 1
	for _, v := range args {
		result = v / result
	}
	return result
}
