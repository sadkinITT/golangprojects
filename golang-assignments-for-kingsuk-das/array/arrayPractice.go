package main

import "fmt"

func main() {
	var size int
	fmt.Println("Enter the size of the array:")
	fmt.Scan(&size)
	fmt.Print("\n")
	var arr [5]int
	var i int
	for i = 0; i < size; i++ {
		fmt.Scanln(&arr[i])
	}
	i = 0
	fmt.Println("Print the array:")
	for i = 0; i < size; i++ {
		fmt.Println(arr[i])
	}
}
