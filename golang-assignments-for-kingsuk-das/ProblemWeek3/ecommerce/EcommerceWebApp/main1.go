package main

import (
	"net/http"

	"github.com/EcommerceWebApp/products"
)

// func addCart(w http.ResponseWriter, r *http.Request) {
// 	io.WriteString(w, "item is added in cart")
// }

func main() {
	//fileServer := http.FileServer(http.Dir("staticcss"))
	http.Handle("/", http.FileServer(http.Dir("home")))
	//http.Handle("/staticcss/", http.StripPrefix("/staticcss/", fileServer))
	http.Handle("/addCart.html", http.FileServer(http.Dir("addcart")))
	http.HandleFunc("/addProducts", products.Handler)
	http.HandleFunc("/orderDetails.html", products.HandlerOrder)
	http.ListenAndServe(":4047", nil)
}
