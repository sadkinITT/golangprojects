package products

import (
	"context"
	"log"
	"sync"

	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client *mongo.Client
var once sync.Once

type Repository interface {
	Add(p *Products) error
	Get() ([]Products, error)
}

type repo struct{}

func getClient() *mongo.Client {
	once.Do(func() {
		clientoptions := options.Client().ApplyURI("mongodb://localhost:27017")
		cli, err := mongo.Connect(context.TODO(), clientoptions)
		if err != nil {
			log.Panic("Error", err)

		}
		client = cli
	})

	return client
}

func (r *repo) Add(p *Products) error {
	client := getClient()
	collection := client.Database("EcoProducts").Collection("products")
	_, err := collection.InsertOne(context.TODO(), p)
	return err
}

func (r *repo) Get() ([]Products, error) {
	client := getClient()
	var productlist []Products
	collection := client.Database("EcoProducts").Collection("products")
	cur, err := collection.Find(context.TODO(), bson.D{})
	if err != nil {
		log.Panic("Could not find from database")
		return nil, err
	}
	for cur.Next(context.TODO()) {
		var p Products
		if err := cur.Decode(&p); err != nil {
			panic(err)
		}
		productlist = append(productlist, p)
	}
	return productlist, nil
}
func NewRepo() Repository {
	return &repo{}
}
