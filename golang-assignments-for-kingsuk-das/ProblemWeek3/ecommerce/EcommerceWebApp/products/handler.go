package products

import (
	"encoding/json"
	"net/http"
)

func addProducts(p *Products) error {
	r := NewRepo()
	return r.Add(p)
}

func getProducts() []Products {
	r := NewRepo()
	el, err := r.Get()
	if err != nil {
		panic(err)
	}
	return el
}

// func readForm(r *http.Request) *Products {
// 	r.ParseForm()
// 	products := new(Products)
// 	decoder := schema.NewDecoder()
// 	decodeErr := decoder.Decode(products, r.PostForm)
// 	if decodeErr != nil {
// 		log.Printf("error mapping parsed form data to struct : ", decodeErr)
// 	}
// 	return products
// }
func Handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch method := r.Method; method {
	case "GET":
		//p := readForm(r)
		var p Products
		//p = readForm(r)
		vals := r.URL.Query()
		ProductID, ProductIDok := vals["ProductID"]
		ProductName, ProductNameok := vals["ProductName"]
		Price, Priceok := vals["Price"]
		if ProductIDok && ProductNameok && Priceok {
			p = Products{ProductID[0], ProductName[0], Price[0]}
		}

		// if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		// 	panic(err)
		// }
		addProducts(&p)
	case "POST":
		var p Products
		if err := r.ParseForm(); err != nil {
			panic(err)
		}
		ProductID := r.FormValue("ProductID")
		ProductName := r.FormValue("ProductName")
		Price := r.FormValue("Price")
		p = Products{ProductID, ProductName, Price}
		addProducts(&p)
	}
}

func HandlerOrder(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if el := getProducts(); el == nil {
		json.NewEncoder(w).Encode("Error getting the data")
	} else {
		json.NewEncoder(w).Encode(el)
	}
}
