package main

import "fmt"

func main() {
	k := [5]int{0, 1, 2, 3, 4}
	slice := k[1:4]
	fmt.Println(slice)
	slice1 := make([]int, 5, 10)
	for x := 0; x < 5; x++ {
		fmt.Scanln(&slice1[x])
	}
	for x := 0; x < len(slice1); x++ {
		fmt.Print(slice1[x])
	}
	fmt.Println()
	a := make([]int, 5)
	printSlice("a", a)

	b := make([]int, 0, 5)
	printSlice("b", b)

	c := b[:2]
	printSlice("c", c)

	d := c[2:5]
	printSlice("d", d)
}

func printSlice(s string, x []int) {
	fmt.Printf("%s len=%d cap=%d %v\n",
		s, len(x), cap(x), x)
}
