package main

import "fmt"

func sumOfNumbers(arr []int, size int) int {
	var sum int = 0
	for x := 0; x < size; x++ {
		sum += arr[x]
	}
	return sum
}

func averageOfNumbers(sum, size int) float64 {
	return float64(sum / size)
}

func reversingArrayofNumbers(arr []int, size int) {
	for x := size - 1; x >= 0; x-- {
		fmt.Print(arr[x], " ")
	}
}
func main() {
	var size int
	fmt.Scan(&size)
	var arr []int
	for x := 0; x < size; x++ {
		fmt.Scanln(&arr[x])
	}
	sum := sumOfNumbers(arr, size)
	average := averageOfNumbers(sum, size)
	fmt.Println(sum)
	fmt.Println(average)
	reversingArrayofNumbers(arr, size)

}
