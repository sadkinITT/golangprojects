package main

import "fmt"

//Variadic parameters example
func add(args ...int) int {
	total := 0
	for _, i := range args {
		total += i
	}
	return total
}

func greatestNumber(args ...int) int {
	number := -99999
	for _, i := range args {
		if number < i {
			number = i
		}
	}
	return number
}

func sum(arr []int) int {
	total := 0
	for _, i := range arr {
		total += i
	}
	return total
}

func recoveringfunc() {
	str := recover()
	fmt.Println(str)
}

func main() {
	k := add(1, 2, 3)
	fmt.Println("The addition of 1,2,3 is", k)

	//Closure example
	s := func(x, y int) int {
		if x > y {
			return x - y
		} else if x < y {
			return y - x
		} else {
			return 0
		}
	}
	fmt.Println("The substraction of two numbers are:", s(1, 2))

	m := sum([]int{1, 2, 3})
	fmt.Println(m)

	g := greatestNumber(1, 2, 3, 456, 5, 6, 8786)
	fmt.Println(g)
	defer recoveringfunc()
	panic("runtime error created")

}
