package main

import (
	"fmt"
	"strings"
)

func main() {
	var s1 string = "Um,br,ela"
	var s2 string = "Um"
	fmt.Println(strings.Compare(s1, s2))
	fmt.Println(strings.Contains(s1, s2))
	fmt.Println(strings.ContainsAny(s1, s2))
	fmt.Println(strings.Count(s1, s2))
	//var s3 []string
	s3 := strings.Split(s1, ",")
	fmt.Println(s3)
}
