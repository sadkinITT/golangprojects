package main

import (
	"net/http"

	"github.com/TestDB/handler"
)

func main() {
	http.HandleFunc("/", handler.ShowProduct)
	http.HandleFunc("/insert", handler.InsertProduct)
	http.HandleFunc("/changeProduct", handler.ChangeProduct)
	http.HandleFunc("/updateProducts", handler.UpdateProduct)
	http.ListenAndServe(":5007", nil)
	//http.HandleFunc("/insert",handler.InsertProduct)
}
