package handler

import (
	"fmt"
	"net/http"
)

//Connect to database
//var session, err = mgo.Dial(conStr)

//ShowProduct func show details of product to web
func ShowProduct(w http.ResponseWriter, r *http.Request) {
	s := NewRepo()
	iter := s.Show()
	result := Product{}
	//var r1 []employee
	for iter.Next(&result) {
		//r1 = append(r1, result)
		ParsingTemplate(result, w, r)

	}

}

func ChangeProduct(w http.ResponseWriter, r *http.Request) {
	var p Product
	vals := r.URL.Query()
	ProdID, ProdIDok := vals["ProdID"]
	ProdName, ProdNameok := vals["ProdName"]
	ProdDsp, ProdDspok := vals["ProdDsp"]
	ProdPrice, ProdPriceok := vals["ProdPrice"]
	ProdQuantity, ProdQuantityok := vals["ProdQuantity"]
	if ProdIDok && ProdNameok && ProdDspok && ProdPriceok && ProdQuantityok {
		p = Product{ProdID[0], ProdName[0], ProdDsp[0], ProdPrice[0], ProdQuantity[0]}
	}
	ChangeParsingTemplate(p, w, r)
}

//InsertProduct func insert product into Database
func InsertProduct(w http.ResponseWriter, r *http.Request) {

	i := NewRepo()
	i.InsertD()
}

func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	u := NewSecRepo()
	w.Header().Set("Content-Type", "application/json")
	var p Product
	if err := r.ParseForm(); err != nil {
		panic(err)
	}
	ProdID := r.FormValue("ProdID")
	ProdName := r.FormValue("ProdName")
	ProdDsp := r.FormValue("ProdDsp")
	ProdPrice := r.FormValue("ProdPrice")
	ProdQuantity := r.FormValue("ProdQuantity")
	fmt.Println("id", ProdID)
	fmt.Println("ProdName", ProdName)
	fmt.Println("ProdDsp", ProdDsp)
	fmt.Println("ProdPrice", ProdPrice)
	fmt.Println("ProdQuantity", ProdQuantity)
	p = Product{ProdID, ProdName, ProdDsp, ProdPrice, ProdQuantity}
	u.Update(ProdID, &p)
}
