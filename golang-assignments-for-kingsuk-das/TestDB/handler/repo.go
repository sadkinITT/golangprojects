package handler

import (
	"fmt"
	"log"

	"gopkg.in/mgo.v2"
)

const (
	conStr         = "localhost:27017"
	dbName         = "ecommerce"
	prodCollection = "products"
)

type Repository interface {
	Show() *mgo.Iter
	InsertD()
}

func getSession() (*mgo.Session, error) {
	var session, err = mgo.Dial(conStr)
	if err != nil {
		log.Fatal(err)
		return nil, err
	} else {
		fmt.Println("connected")
	}
	//defer session.Close()
	return session, nil
}

type repo struct{}

func (r *repo) Show() *mgo.Iter {
	session, err := getSession()
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	c := session.DB(dbName).C(prodCollection)

	iter := c.Find(nil).Iter()
	return iter
}

func (r *repo) InsertD() {

	//Switch the session to a monotonic behaviour
	session, err := getSession()
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	c := session.DB(dbName).C(prodCollection)

	doc := Product{
		"135",
		"pen",
		"pen",
		"20",
		"15",
	}

	//insert a category object
	err = c.Insert(&doc)
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Inserted!!!")
	}
}
func NewRepo() Repository {
	return &repo{}
}
