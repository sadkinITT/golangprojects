package handler

//Product holds deatails about product
type Product struct {
	ProdID       string `json:"prodid"`
	ProdName     string `json:"prodname"`
	ProdDsp      string `json:"proddsp"`
	ProdPrice    string `json:"prodprice"`
	ProdQuantity string `json:"prodquantity"`
}
