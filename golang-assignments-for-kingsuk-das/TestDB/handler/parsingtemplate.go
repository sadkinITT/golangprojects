package handler

import (
	"html/template"
	"net/http"
)

func ParsingTemplate(p Product, w http.ResponseWriter, r *http.Request) {
	parsedTemplate, errparsedTemplate := template.ParseFiles("homepage/homep.html")
	if errparsedTemplate != nil {
		panic(errparsedTemplate)
	}
	parseerr := parsedTemplate.Execute(w, p)
	if parseerr != nil {
		panic(parseerr)
	}
}

func ChangeParsingTemplate(p Product, w http.ResponseWriter, r *http.Request) {
	cparsedTemplate, errcparsedTemplate := template.ParseFiles("updateProduct/updateProduct.html")
	if errcparsedTemplate != nil {
		panic(errcparsedTemplate)
	}
	cparseerr := cparsedTemplate.Execute(w, p)
	if cparseerr != nil {
		panic(cparseerr)
	}
}
