package handler

import (
	"fmt"
	"log"

	"gopkg.in/mgo.v2"

	"gopkg.in/mgo.v2/bson"
)

const (
	conStrs         = "localhost:27017"
	dbNames         = "ecommerce"
	prodCollections = "products"
)

func getSessions() (*mgo.Session, error) {
	var session, err = mgo.Dial(conStrs)
	if err != nil {
		log.Fatal(err)
		return nil, err
	} else {
		fmt.Println("connected")
	}
	//defer session.Close()
	return session, nil
}

type SecRepository interface {
	Update(id string, p *Product)
}

type secrepo struct{}

// var client *mongo.Client
// var once sync.Once

// func getClient() *mongo.Client {
// 	once.Do(func() {
// 		clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
// 		cli, err := mongo.Connect(context.TODO(), clientOptions)
// 		if err != nil {
// 			log.Panic("Error", err)

// 		}
// 		client = cli
// 	})

// 	return client
// }

func (s *secrepo) Update(id string, p *Product) {
	fmt.Println("Inside the DAO")
	// client := getClient()
	// collection := client.Database("ecommerce").Collection("products")
	session, err := getSessions()
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	c := session.DB(dbNames).C(prodCollections)
	var changeinfo *mgo.ChangeInfo
	fmt.Println("id", p.ProdID)
	fmt.Println("ProdName", p.ProdName)
	fmt.Println("ProdDsp", p.ProdDsp)
	fmt.Println("ProdPrice", p.ProdPrice)
	fmt.Println("ProdQuantity", p.ProdQuantity)
	filter := bson.M{"prodid": id}
	update := bson.M{"$set": bson.M{"prodid": id, "prodname": p.ProdName, "proddsp": p.ProdDsp, "prodprice": p.ProdPrice, "prodquantity": p.ProdQuantity}}
	changeinfo, errchangeinfo := c.Upsert(filter, update)
	//upsert := true

	// _, err := collection.UpdateOne(context.TODO(), filter, update)
	if errchangeinfo != nil {
		panic(err)
	}
	fmt.Println("Number of documents updated in database are :: %d", changeinfo.Updated)
}

func NewSecRepo() SecRepository {
	return &secrepo{}
}
