package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const path = "NOTES.txt"

//type File struct{}
func closingFile(f *os.File) {
	f.Close()
}

func errorCheck(e error) bool {
	if e != nil {
		return true
	}
	return false
}

func createFile() (*os.File, error) {
	file, err := os.Create(path)
	if errorCheck(err) {
		return nil, err
	}
	fmt.Println("File created")
	defer closingFile(file)
	return file, nil
}

func openFileForWrite() (*os.File, error) {
	file, err := os.OpenFile(path, os.O_WRONLY, 0644)
	if errorCheck(err) {
		return nil, err
	}
	return file, err
}

func openFileForRead() (*os.File, error) {
	file, err := os.OpenFile(path, os.O_RDONLY, 0666)
	if errorCheck(err) {
		return nil, err
	}
	return file, err
}

func writeFile() {
	file, err := openFileForWrite()
	if errorCheck(err) {
		panic(err)
	}
	for i := 1; i <= 100; i++ {
		_, err = file.WriteString(fmt.Sprintf("%d\n", i))
		if errorCheck(err) {
			fmt.Printf("error writing string: %v", err)
		}
	}
	fmt.Println("writing done")
	defer closingFile(file)
}

func readFile() {
	file, err := openFileForRead()
	if errorCheck(err) {
		panic(err)
	}
	if errorCheck(err) {
		log.Fatal(err)
	}
	defer closingFile(file)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); errorCheck(err) {
		log.Fatal(err)
	}

}

func main() {
	createFile()
	writeFile()
	readFile()
}
