package main

import "fmt"

type employees struct {
	firstname string
	lastname  string
	age       int
	email     string
}

func main() {
	var firstname, lastname, email string
	var age, num int
	var e []employees
	fmt.Println("Enter the number of employees:")
	fmt.Scan(&num)
	fmt.Println("Enter the elements in structure:")
	for i := 0; i < num; i++ {
		fmt.Scanln(&firstname)
		fmt.Scanln(&lastname)
		fmt.Scanln(&age)
		fmt.Scanln(&email)
		y := employees{firstname, lastname, age, email}
		e = append(e, y)
	}
	for i := 0; i < num; i++ {
		fmt.Println(e[i])
	}

}
