package main

import (
	"fmt"
)

type employee struct {
	firstname string
	lastname  string
	age       int
	email     string
}

func (e *employee) GiveDetails() {
	fmt.Println("Employee name is:", e.firstname, " ", e.lastname,
		"whose age is", e.age, "and the email id of employee is", e.email)
}

func main() {
	var number, age int
	var firstname, lastname, email string
	fmt.Scanln(&number)
	//e := make([]employee, number, 100)
	var e []employee
	i := 0
	for i < number {
		fmt.Scanln(&firstname)
		fmt.Scanln(&lastname)
		fmt.Scanln(&age)
		fmt.Scanln(&email)
		y := employee{firstname, lastname, age, email}
		e = append(e, y)
		i++
	}
	for j := 0; j < number; j++ {
		//fmt.Println(e[j])
		e[j].GiveDetails()
	}

	/*x := employee{"Kingsuk","Das",24,"kingsuk.das@intimetec.com"}
	fmt.Println(x)
	var number int
	fmt.Scanln(&number)
	for _,i := range number{
		employee.firstname = fmt.Scanln(&)
	}*/
}
