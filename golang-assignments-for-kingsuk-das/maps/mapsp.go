package main

import "fmt"

func main() {
	x := map[string]int{
		"Kate":     28,
		"John":     42,
		"Derek":    45,
		"Kingsley": 29,
	}
	fmt.Println(x)
	x["Mohan"] = 23
	fmt.Println(x)
	delete(x, "Derek")
	fmt.Println(x)
	y := map[int]string{
		1: "K",
		2: "I",
		3: "N",
		4: "G",
		5: "S",
		6: "U",
		7: "K",
	}
	fmt.Print(y)
	fmt.Println()
	fmt.Println(y[1])
	z := make(map[string]map[int]string)
	z["first"] = map[int]string{
		1: "A",
		2: "B",
	}
	z["second"] = map[int]string{
		1: "C",
		2: "D",
	}
	z["third"] = map[int]string{
		1: "E",
		2: "F",
	}
	z["fourth"] = map[int]string{
		1: "G",
		2: "H",
	}
	fmt.Println(z)
	var m map[int]string
	m = map[int]string{
		1: "A",
		2: "B",
		3: "C",
	}
	fmt.Println(m)
}
