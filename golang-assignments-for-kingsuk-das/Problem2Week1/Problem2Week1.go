package main

import (
	"fmt"
)

func findMax(arr [6]int) int {
	var max int = -99999
	for x := 0; x < 6; x++ {
		if max < arr[x] {
			max = arr[x]
		}
	}
	return max
}

func findMin(arr [6]int) int {
	var min int = 99999
	for x := 0; x < 6; x++ {
		if min > arr[x] {
			min = arr[x]
		}
	}
	return min
}
func differanceBetweenMaxAndMin(max, min int) {
	fmt.Println("The difference of max and min is", (max - min))
}
func main() {
	var arr [6]int
	var x int
	for x = 0; x < 6; x++ {
		fmt.Scanln(&arr[x])
	}

	max1 := findMax(arr)
	min1 := findMin(arr)
	differanceBetweenMaxAndMin(max1, min1)
}
