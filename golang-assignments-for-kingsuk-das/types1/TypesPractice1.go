package main

import "fmt"

func add(x float64, y float64) float64 {
	return x + y
}

func identify(x int64) {
	switch x {
	case 1:
		fmt.Println("The value is", x)
	case 2:
		fmt.Println("The value is ", x)
	}

}

func forloop(x int) {
	var y int = 0
	for ; y < x; y++ {
		fmt.Println(x)
	}
	fmt.Println("number of times printed", y)
}

func forrangeloop(nums []int) {
	for k, v := range nums {
		fmt.Println(k, "-->", v)
	}
}

func ageValidator(age int) {
Loop:
	if age < 17 {
		fmt.Println("not eligable")
		fmt.Println("Enter your age:")
		goto Loop
	}
}

func main() {
	//var num1 float64 = 5.6
	//var num2 float64 = 6.1
	//var integer int64
	//k := 30
	var n [5]int

	for x := 0; x < 5; x++ {
		fmt.Scanln(&n[x])
	}
	forrangeloop(n[:])
	forrangeloop(n[1:4])
	var ar = [2][3]int{
		{1, 2, 3},
		{4, 5, 6}}

	for k := 0; k < 2; k++ {
		for j := 0; j < 3; j++ {
			fmt.Print(ar[k][j])
		}
		fmt.Println()
	}
	var c []int = ar[0]
	fmt.Println(c)
	/*if num1 > num2 {
		fmt.Println(add(num1, num2))
	} else {
		fmt.Println(add(num2, num1))
	}

	fmt.Scanln(&integer)
	identify(integer)

	switch k {
	case 10:
		fmt.Println("A")
		fallthrough
	case 20:
		fmt.Println("B")
		fallthrough
	case 30:
		fmt.Println("C")
		fallthrough
	case 40:
		fmt.Println("D")
		fallthrough
	case 50:
		fmt.Println("E")
		fallthrough
	default:
		fmt.Println("F")
	}
	forloop(k)*/

}
