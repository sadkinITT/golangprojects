package main

import (
	"fmt"
)

type DetailsEmployee interface {
	GiveDetails()
}

type seniorexecutive struct {
	firstname string
	lastname  string
	age       int
	email     string
	location  string
}

type juniorexecutive struct {
	firstname string
	lastname  string
	age       int
	email     string
	location  string
}

func (s seniorexecutive) GiveDetails() {
	fmt.Println("Employee name is:", s.firstname, " ", s.lastname,
		"whose age is", s.age, "and the email id of employee is", s.email)
}

func (j juniorexecutive) GiveDetails() {
	fmt.Println("Employee name is:", j.firstname, " ", j.lastname,
		"whose age is", j.age, "and the email id of employee is", j.email)
}

func welcome(d DetailsEmployee) {
	fmt.Println("Welcome")
	d.GiveDetails()
}

func main() {
	var t []DetailsEmployee
	t[0] = seniorexecutive{"A", "B", 43, "a.b@itt.com", "Bangalore"}
	t[1] = juniorexecutive{"A", "B", 43, "a.b@itt.com", "Bangalore"}
	for i := 0; i < 2; i++ {
		welcome(t[i])
	}

}
