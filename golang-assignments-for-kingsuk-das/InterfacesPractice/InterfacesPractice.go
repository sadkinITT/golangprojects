package main

import (
	"fmt"
)

/*type employee struct {
	firstName string
	lastName  string
	age       int
	email     string
}*/

type DetailsEmployee interface {
	GiveDetails()
	GivefirstName()
	GivelastName()
	Welcome()
}

type seniorexecutive struct {
	firstName string
	lastName  string
	age       int
	email     string
	location  string
}

type juniorexecutive struct {
	firstName string
	lastName  string
	age       int
	email     string
	location  string
}

var t1 []seniorexecutive
var t2 []juniorexecutive

func (s seniorexecutive) GiveDetails() {
	fmt.Println("Employee name is:", s.firstName, " ", s.lastName,
		"whose age is", s.age, "and the email id of employee is", s.email)
}

func (s seniorexecutive) GivefirstName() {
	fmt.Println("Employee first name is", s.firstName)
}

func (s seniorexecutive) GivelastName() {
	fmt.Println("Employee last name is", s.lastName)
}

func (j juniorexecutive) GiveDetails() {
	fmt.Println("Employee name is:", j.firstName, " ", j.lastName,
		"whose age is", j.age, "and the email id of employee is", j.email)
}

func (j juniorexecutive) GivefirstName() {
	fmt.Println("Employee first name is", j.firstName)
}

func (j juniorexecutive) GivelastName() {
	fmt.Println("Employee last name is", j.lastName)
}

func (s seniorexecutive) Welcome(s1 seniorexecutive) {
	fmt.Println("Welcome")
	s1.GivefirstName()
	s1.GivelastName()
	s1.GiveDetails()
}

func (j juniorexecutive) Welcome(j1 juniorexecutive) {
	fmt.Println("Welcome")
	j1.GivefirstName()
	j1.GivelastName()
	j1.GiveDetails()
}

func createSeniorExecutive(firstName string, lastName string, age int, email string, location string) {
	tT := seniorexecutive{firstName, lastName, age, email, location}
	t1 = append(t1, tT)
}

func createJuniorExecutive(firstName string, lastName string, age int, email string, location string) {
	tT := juniorexecutive{firstName, lastName, age, email, location}
	t2 = append(t2, tT)
}

func Showseniorexecutive(s seniorexecutive) {
	s.Welcome(s)
}

func Showjuniorexecutive(j juniorexecutive) {
	j.Welcome(j)
}

func main() {
	var number, age int
	var firstName, lastName, email, location string
	fmt.Scanln(&number)
	i := 0
	for i < number {
		fmt.Scanln(&firstName)
		fmt.Scanln(&lastName)
		fmt.Scanln(&age)
		fmt.Scanln(&email)
		fmt.Scanln(&location)
		if i%2 == 0 {
			fmt.Println("Senior executive")
			createSeniorExecutive(firstName, lastName, age, email, location)
		} else {
			fmt.Println("Junior executive")
			createJuniorExecutive(firstName, lastName, age, email, location)
		}
		i++
	}
	for i := 0; i < len(t1); i++ {
		Showseniorexecutive(t1[i])
	}
	for i := 0; i < len(t2); i++ {
		Showjuniorexecutive(t2[i])
	}

}
