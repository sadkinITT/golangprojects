package main

import "fmt"

func sumOfDigits(number int) int {
	var remain int
	var sum int = 0
	for number != 0 {
		remain = number % 10
		number = number / 10
		sum += remain
	}
	return sum
}
func main() {
	var number int
	fmt.Scanln(&number)
	fmt.Println("sum is:", sumOfDigits(number))

}
